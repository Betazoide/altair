# Technical test
## Infrastructure creation
To create the infrastructure I'm going to use AWS as the Cloud provider. We need a VM to store the MySQL database and a Kubernetes cluster to run the application. Due I'm using the free AWS tier, only the database will be deployed in the cloud. Kubernetes cluster will be replaced by `minikube` to simulate it.

The subnet, the security groups and the VPC are already created in AWS. To execute the infra creation you just need to run terraform:
```bash
terraform init
terraform apply
```
Terraform will use the file `main.tf` to deploy the VM
## Provisioning the MySQL database
To provision the database I'm going to use Ansible and a role downloaded from Ansible Galaxy to install MySQL, creating a user and a database.
```bash
ansible-galaxy install -r requirements.yml
ansible-playbook -i inventory dabatase-deploy.yml
```
Under `group_vars/all.yml` you will find the variables needed to execute this rol and create the user and the database.
## Deploying the cluster
Due the AWS free tier just allows some compute hours by free per month, I'm going to use `minikube` to simulate the kubernetes cluster. Also will be used to store more services that I need to deploy.
First we need to start 
```bash
minikube start
minikube addons enable metrics-server
minikube dashboard
```
With the last command the Kubernetes web UI its launched so we can have the visual about what it's happening.
## Deploying the application
For this test I'm going to deploy a Wildfly application into the cluster. The application will return a webpage with a simple text.

You can find the code by following this [link](https://gitlab.com/Betazoide/example). The target is the application named "example". I have also included in this repo the Dockerfile and a configuration for Wildfly that we need to use to be able to monitoring the application.
### Compile
The first step it's to compile the application. In the previous example repo you need to run:
```bash
mvn -f example/ -e clean package
```
 ### Creating and publishing the Docker image
 And now its turn to build the Docker image that will be used to be deployed into Kubernetes:
```bash
docker build --tag=wildfly-test .
```
The Dockerfile contains the base image `jboss/wildfly`, the add of the application and the `standalone.xml`. This last one has a little modification in order to allow external connections to the management port. This will be used to configure Prometheus.

The image is ready to be uploaded into the Docker registry:
```bash
export IMAGEID=$(docker images | grep wildfly-test | tr -s ' ' | cut -d' ' -f3)
docker tag $IMAGEID betazoide/altair-test:1.0
docker push betazoide/altair-test:1.0
```
### Deploying the app
To deploy the application I'm going to use `helm` and a Wildfly chart, as follows:
```bash
helm install wfapp -f values-wf.yaml bitnami/wildfly
```
The file [values-wf.yaml](https://gitlab.com/Betazoide/altair/-/blob/master/values-wf.yaml) contains chart modifications, like the image to be deployed, but not all the configuration it's made by the chart, like for example, in this case, auto-scalling is not included.

To enable the auto-scalling, you need to run:
```bash
kubectl autoscale deployment wfapp-wildfly --cpu-percent=30 --min=1 --max=3
```
For this test I have choose a minimum of 1 pod and maximum 3. To check the CPU usage, we can run:
```bash
kubectl get hpa
```
And now it's time to check that the application is working. To test it, first, we need to forward the service in order to be able to access from the outside of the cluster:
```bash
kubectl port-forward service/wfapp-wildfly 8080:80
```
And we can found under `http://localhost:8080` the webpage.
## Builds
To have a pipeline where to automatically build new versions of the application, I have choose Jenkins as the build server.
Two jobs are created:

 1. **BuildWFApp**: to build the application on every change on master. This master branch needs to be protected so nothing can be commit directly there, just allows to merge a branch on it. As Jenkins has been installed locally on my computer, Gitlab can't trigger a new build on each merge. Because of that, I have decide to use the *SCM* plugin that checks every X minutes if there is any change on the branch you choose (in this case, master). The versioning will be based on the build number, so we will avoid any collision.The script executed is the next one:
```bash
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
mvn -e clean package -f example/
docker build --tag=wildfly-test .
export IMAGEID=$(docker images | grep wildfly-test | tr -s ' ' | cut -d' ' -f3)
docker tag $IMAGEID betazoide/altair-test:${BUILD_NUMBER}.0
docker push betazoide/altair-test:${BUILD_NUMBER}.0
```
 2. **DeployWFApp**: with this job a specific version of the application can be deployed. With the *Build With Parameters* plugin we can choose a version number before start the job. The job is as follows:
```bash
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
helm upgrade wfapp bitnami/wildfly -f values-wf.yaml --set image.tag=$WFAPPVERSION.0
```
## Monitoring and alerting
To monitor the application I have choose Prometheus, which can be also deployed with `helm` and after that, some modifications needs to be done in order to monitor WildFly:
```bash
helm install monit prometheus-community/prometheus
```
WildFly gives metrics by default under `:9990/metrics`, so we need to configure that into Prometheus.

To edit the config map:
```bash
kubectl edit cm monit-prometheus-server
```
And the configuration to be added:
```yaml
- job_name: 'wfapp'
  tls_config:
    insecure_skip_verify: true
  static_configs:
  - targets:
    - wfapp-wildfly:9990
```
To configure the alerts, we need to modify again the config map with the alerts we would like to receive:
```yaml
  - alert: HostOutOfDiskSpace
    expr: (node_filesystem_avail_bytes * 100) / node_filesystem_size_bytes < 10 and ON (instance, device, mountpoint) node_filesystem_readonly == 0
    for: 2m
    labels:
      severity: warning
    annotations:
      summary: Host out of disk space (instance {{ $labels.instance }})
      description: Disk is almost full (< 10% left)\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}
```
To be able to see the Prometheus panel, we need to run:
```bash
export POD_NAME=$(kubectl get pods --namespace default -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace default port-forward $POD_NAME 9093
```
And get into it by going to `http://localhost:9093`.
## Logs
To collect the logs we are going to use `fluentd`:
```bash
helm install collect bitnami/fluentd
```
With this, we will collect all the logs and can be send into, for example to ElasticSearch to be shown in Kibana.