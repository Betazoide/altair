terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_instance" "app_server" {
  ami           = "ami-096fda3c22c1c990a"
  instance_type = "t2.micro"
  key_name      = "ks27dr"

  subnet_id = "subnet-06e7f250ec273c865"
  security_groups = [ "sg-0cce9dee8d118f355" ]

  tags = {
    Name = "WPDatabase"
  }
}

